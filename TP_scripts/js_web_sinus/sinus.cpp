#include <emscripten/bind.h>
#include <cmath>

struct Color {
    uint8_t _r;
    uint8_t _g;
    uint8_t _b;
    uint8_t _a;
};

class Sinus {

    private:
        int _width;
        int _height;
        std::vector<Color> _data;

    public:
        Sinus(int width, int height) : 
            _width(width), _height(height), _data(width*height) {
        }

        emscripten::val update(float a, float b) { 
            int i = 0;
            for (Color & c : _data){
                c = {(uint8_t)(sinus(1.4, a, b)/**255*/), 0, 0, 255};
                i++;
            }
                

            size_t s = _data.size()*4;
            uint8_t * d = (uint8_t *) _data.data();
            return emscripten::val(emscripten::typed_memory_view(s, d));
        }

        double sinus(int x, float a, float b){
            return x;
            //return sin((2*M_PI*((a*x)+b)));
            //return 1;
            //return b;
        }
};



EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::class_<Sinus>("Sinus")
        .constructor<int, int>()
        .function("update", &Sinus::update)
        .function("sinus", &Sinus::sinus);
}

