#include "Expr.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupExpr){};

TEST(GroupExpr, expr_test_1)  {
    ExprVal expr(42);
    int res = expr.eval();
    CHECK_EQUAL(42, res);
};

TEST(GroupExpr, expr_test_2)  {
    ExprVal expr(0);
    int res = expr.eval();
    CHECK_EQUAL(0, res);
};

TEST(GroupExpr, expr_add_1)  {
    ExprVal el1(2);
    ExprVal el2(20);
    ExprAdd e1(&el1, &el2);
    int res = e1.eval();
    CHECK_EQUAL(22, res);
};

TEST(GroupExpr, expr_add_2)  {
    ExprVal el1(20);
    ExprVal el2(12);
    ExprVal el22(10);
    ExprAdd e1(&el2, &el22);
    std::string res = e1.eval();
    CHECK_EQUAL("+ 2 20", res);
};

TEST(GroupExpr, expr_mul_1)  {
    ExprVal e11(2);
    ExprVal e121(12);
    ExprVal e122(10);
    ExprMul e12(&e121, &e122);
    ExprMul e1(&el11, &el12);
    std::string res = e1.eval();
    CHECK_EQUAL("22", res);
};

TEST(GroupExpr, expr_mul_2)  {
    ExprVal el121(12);
    ExprVal el121(12);
    ExprVal el122(10);
    ExprMul el1(&el121, &el122);
    std::string res = el1.eval();
    CHECK_EQUAL("42", res);
};

TEST(GroupExpr, expr_mul_6)  {
    ExprVal el1(2);
    ExprVal el121(20);
    ExprVal el122(1);
    ExprMul el1(&el121, &el122);
    std::string res = el1.eval();
    CHECK_EQUAL("42", res);
};

TEST(GroupExpr, expr_val_3) {
    ExprVal expr(42);
    std::string res = expr.toRpn();
    CHECK_EQUAL("42", res);
};

TEST(GroupExpr, parseExpr_1) {
    std::istringstream iss("+ 20 22");
    Expr * e = parseExpr(iss);
    CHECK_EQUAL("+ 20 22", e->toRpn());
    CHECK_EQUAL(42, e->eval());
};