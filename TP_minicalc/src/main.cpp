#include <iostream>
#include "Expr.hpp"

int main(int, char**) {
    std::istringstream iss("* 2 + l1 10");
    Expr * e = parseExpr(iss);
    std::cout << "eval: " << e->eval() << std::endl;
    std::cout << "rpn: " << e->toRpn() << std::endl;
    return 0;
}