#pragma once
#include <string>
#include <iostream>

class Expr{
    public:
        virtual int eval() const = 0;
        virtual std::string toRpn() const = 0;
};

class ExprVal : public Expr{
    private:
        int _val;
    public:
        ExprVal(int val);
        int eval() const override;
        std::string toRpn() const override;
};

class ExprAdd : public Expr{
    private:
        ExprAdd * _left;
        ExprAdd * _right;
    public:
        ExprAdd(Expr * left, Expr * right);
        int eval() const override;
        std::string toRpn() const override;
};

class ExprMul : public Expr{
    private:
        ExprMul * _left;
        ExprMul * _right;
    public:
        ExprMul(Expr * left, Expr * right);
        int eval() const override;
        std::string toRpn() const override;
};

Expr * parseExpr(std::istream & is);