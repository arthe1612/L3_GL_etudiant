#include "Route.hpp"

bool Route::operator==(const Route & r) const {
    return ((this->villeA_ == r.villeA_) && (this->villeB_ == r.villeB_) && (this->distance_ == r.distance_));
}

bool Route::operator!=(const Route & r) const {
    return not operator==(r);
}

