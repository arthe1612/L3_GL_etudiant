.. Drunk_Player documentation master file, created by
   sphinx-quickstart on Wed Apr  8 11:04:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Drunk_Player's documentation!
========================================

Sommaire
++++++++++++++++++++++++++++++++

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation

.. codeblocks:: cpp

   int main() {
      return 0;
   }



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
