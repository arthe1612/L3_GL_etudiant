#include "Fibo.hpp"
#include <assert.h>
#include <string>

int fibo(int n, int f0, int f1) {
    if (!n < 0) throw std::string("erreur -> n < 0");
    int r = n<=0 ? f0 : fibo(n-1, f1, f1+f0);
    return r;
}