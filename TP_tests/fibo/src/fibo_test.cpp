#include "Fibo.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, Fibo_test1)  {
    int tab [] = {0, 1, 1, 2, 3};
    for (int i=0; i<5; i++){
         CHECK_EQUAL(fibo(i), tab[i]);
    }
};

TEST(GroupFibo, Fibo_test2)  {
    try{
        int result = fibo(-1, 0, 0);
        FAIL("Exception non levée");
    }catch(const std::string & e){
            CHECK("erreur -> n < 0" == e )
    }
    catch(...){
        FAIL("Exception pas bono type");
    }
};