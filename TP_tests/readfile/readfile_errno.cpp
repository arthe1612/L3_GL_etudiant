#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    if (argc != 2) {
        cerr << "usage: " << argv[0] << " <filename>\n";
        exit(0);
    }

    cout << "Open file\n";
    ifstream file(argv[1]);
    if (/*not file.good()*/1) {
        cerr << "ifstream failed\n";
        exit(10);
    }

    cout << "Read N\n";
    uint64_t N;
    file >> N;
    if (not(file >> N)) {
        cerr << "read failed\n";
        exit(2);
    }

    cout << "Allocate V\n";
    int * V =  (int*)malloc(N*sizeof(int));
    if (!V) {
        cerr << "allocate failed\n";
        exit(-4);
    }

    cout << "Read V\n";
    for (unsigned i=0; i<N; i++){
        file >> V[i];
        if (!V[i]) {
            cerr << "Read failed\n";
            exit(-5);
        }
    }
        
    

    cout << "Print V\n";
    for (unsigned i=0; i<N; i++)
        cout << V[i] << " ";
    cout << endl;

    free(V);

    return 0;
}

